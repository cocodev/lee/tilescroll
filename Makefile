all: ./build/tileScroll.dsk

./build/t.bin: tileScroll.asm	./tileScroll_inc.asm ./diskRoutines.asm
	lwasm --6309 --decb --format=decb --list=./build/tileScroll.lst --symbol-dump=./build/tileScroll.s --symbols --map=./build/tileScoll.map --output=./build/t.bin ./tileScroll.asm

./build/tileScroll.dsk: ./build/t.bin ./t.bas ./data/CKMAP.DTA ./data/PCBLOX.DAT

ifeq ("$(wildcard ./build/tileScroll.dsk)","")
	decb dskini ./build/tileScroll.dsk
endif
	#../../../lst2cmt.exe /SYSTEM coco3h /OFFSET 21F7 /OVERWRITE ./tileScroll.lst "D:\Emulators\Mame\comments\coco3h.cmt"
	decb copy -2 -b -r ./build/t.bin ./build/tileScroll.dsk,T.BIN
	decb copy -2 -b -r ./data/CKMAP.DTA ./build/tileScroll.dsk,CKMAP.DTA
	decb copy -2 -b -r ./data/PCBLOX.DAT ./build/tileScroll.dsk,PCBLOX.DAT
	decb copy -0 -a -r ./t.bas ./build/tileScroll.dsk,T.BAS

clean:
	find ./build -type f \( -name "*.o" -o -name "*.bin" -o -name "*.map" -o -name "*.lst" -o -name "*.s" -o -name "*.dsk" \) -delete
