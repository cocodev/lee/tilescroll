    pragma  undefextern
    pragma  newsource
    pragma  cescapes
    pragma  nodollarlocal

;DEBUG                           equ     1

    org     $0000
playfieldTopLeftX               rmb     2   ; Current top-left X coordinate
playfieldTopLeftY               rmb     2   ; Current top-left Y coordinate

    org     $2000
shouldSkipLoadingData           fcb     0   ; 0 = Load data, != 0 = Skip loading data

    include diskRoutines.asm
    include tileScroll_inc.asm

videoBufferPhysicalBlockOnScreen     rmb     1   ; 
videoBufferPhysicalBlockOffScreen    rmb     1   ; 
verticalOffsetOnScreen          rmb     2   ; 
verticalOffsetOffScreen         rmb     2   ; 
openFileForInput                rmb     2
openFileForOutput               rmb     2
diskIrq                         rmb     2
originalErrorHandlerOpCode      rmb     1
originalErrorHandler            rmb     2
errorHandlerStack               rmb     2
bytesToRead                     rmb     2
bytesReadBuffer                 rmb     2
addressOfFilename               rmb     2
physicalBlock                   rmb     1
physicalOffset                  rmb     2
progressCallback                fdb     $0000

safeMmuLogicalBlock             fcb LOGICAL_4000_5FFF ; MMU block to use temporarily to access physical blocks
safeMmuLogicalBlockAddress      fdb LOGICAL_4000_5FFF * $2000
printChar80x24_CurrentCursorX   fcb 0
printChar80x24_CurrentCursorY   fcb 0

gimeRegister0Value              fcb %01001110         ; 0  = CoCo 3 Mode, 1  = CoCo 1/2 Compatible, 
                                                      ; 1  = MMU enabled (0 = disabled)
                                                      ; 0  = GIME IRQ disabled (1 = enabled)
                                                      ; 0  = GIME FIRQ disabled (1 = enabled)
                                                      ; 1  = Vector RAM at $FEXX enabled (0 = disabled)
                                                      ; 1  = Standard SCS (DISK) Normal (0 = expand)
                                                      ; 10 = ROM Map 32k Internal
                                                      ;      0x = 16K Internal, 16K External
                                                      ;      11 = 32K External - Except Interrupt Vectors
paletteStore                    rmb 16

paletteCga                      fcb %000000 ; 0
                                fcb %001000 ; 1
                                fcb %010000 ; 2
                                fcb %011000 ; 3
                                fcb %100000 ; 4
                                fcb %101000 ; 5
                                fcb %100010 ; 6
                                fcb %111000 ; 7
                                fcb %000111 ; 8
                                fcb %001011 ; 9
                                fcb %010111 ; 10
                                fcb %011011 ; 11
                                fcb %100100 ; 12
                                fcb %101101 ; 13
                                fcb %110110 ; 14
                                fcb %111111 ; 15
keyColumnScans                  fcb 1,1,1,1,1,1,1,1

mapToTileLookup                 fcb $25    ; 00 ($00) = No Floor, Fall Through, Stars shine through
                                fcb $6B    ; 01 ($01) = Floor Current, no item
                                fcb $28    ; 02 ($02) = Blue 4-spike wall
                                fcb $2E    ; 03 ($03) = Floor Red
                                fcb $2D    ; 04 ($04) = Floor Purple
                                fcb $06    ; 05 ($05) = Brown Pipes Wall Horizontal, Floor Top, Connector Bottom
                                fcb $14    ; 06 ($06) = Brown Pipes Wall Vertical, Floor Left, Connector Right
                                fcb $1A    ; 07 ($07) = Brown Pipes Wall Top Left Corner, Floor Outer
                                fcb $1C    ; 08 ($08) = Brown Pipes Wall Top Right Corner, Floor Outer
                                fcb $1B    ; 09 ($09) = Brown Pipes Wall Bottom Left Corner, Floor Outer
                                fcb $08    ; 10 ($0A) = Brown Pipes Wall Bottom Right Corner, Floor Outer
                                fcb $18    ; 11 ($0B) = Brown Pipes Wall Down T
                                fcb $15    ; 12 ($0C) = Brown Pipes Wall Up T
                                fcb $19    ; 13 ($0D) = Brown Pipes Wall Right T
                                fcb $17    ; 14 ($0E) = Brown Pipes Wall Left T
                                fcb $16    ; 15 ($0F) = Brown Pipes Wall Plus
                                fcb $04    ; 16 ($10) = Brown Pipes Wall Horizontal, Black Top, Connector Bottom
                                fcb $26    ; 17 ($11) = Brown Pipes Wall Horizontal, Floor Top, Black Bottom
                                fcb $01    ; 18 ($12) = Brown Pipes Wall Vertical, Black Left, Connector Right
                                fcb $1E    ; 19 ($13) = Brown Pipes Wall Vertical, Floor Left, Black Right
                                fcb $00    ; 20 ($14) = Brown Pipes Wall Top Left Corner, Black Outer
                                fcb $09    ; 21 ($15) = Brown Pipes Wall Top Right Corner, Black Outer
                                fcb $02    ; 22 ($16) = Brown Pipes Wall Bottom Left Corner, Black Outer
                                fcb $1D    ; 23 ($17) = Brown Pipes Wall Bottom Right Corner, Black Outer
                                fcb $20    ; 24 ($18) = Bumper Horizontal
                                fcb $27    ; 25 ($19) = Bumper Vertical
                                fcb $1F    ; 26 ($1A) = Bumper Top Left Corner
                                fcb $21    ; 27 ($1B) = Bumper Top Right Corner
                                fcb $2B    ; 28 ($1C) = Bumper Bottom Left Corner
                                fcb $2C    ; 29 ($1D) = Bumper Bottom Right Corner
                                fcb $37    ; 30 ($1E) = Bumper Down T
                                fcb $38    ; 31 ($1F) = Bumper Up T
                                fcb $36    ; 32 ($20) = Bumper Right T
                                fcb $39    ; 33 ($21) = Bumper Left T
                                fcb $2A    ; 34 ($22) = Bumper Dot
                                fcb $22    ; 35 ($23) = Bumper Vertical Top End Cap
                                fcb $29    ; 36 ($24) = Bumper Vertical Bottom End Cap
                                fcb $32    ; 37 ($25) = Bumper Horizontal Left End Cap
                                fcb $33    ; 38 ($26) = Bumper Horizontal Right End Cap
                                fcb $0C    ; 39 ($27) = Push Left
                                fcb $0D    ; 40 ($28) = Push Right
                                fcb $0E    ; 41 ($29) = Push Up
                                fcb $0B    ; 42 ($2A) = Push Down
                                fcb $10    ; 43 ($2B) = Push Up Left
                                fcb $0F    ; 44 ($2C) = Push Up Right
                                fcb $11    ; 45 ($2D) = Push Down Left
                                fcb $12    ; 46 ($2E) = Push Down Right
                                fcb $0A    ; 47 ($2F) = Door Vertical Pipes
                                fcb $07    ; 48 ($30) = Door Horizontal Pipes
                                fcb $03    ; 49 ($31) = Floor Ice
                                fcb $6B    ; 50 ($32) = Floor Current, No Item, Fall Through
                                fcb $25    ; 51 ($33) = Floor Black?
                                fcb $2F    ; 52 ($34) = Floor Green
                                fcb $13    ; 53 ($35) = Bumper Crazy
                                fcb $24    ; 54 ($36) = Magenet
                                fcb $6B    ; 55 ($37) = Floor Current, No Item, Wall For Enemy, Not Wall For Player
                                fcb $23    ; 56 ($38) = Floor Pink
                                fcb $6B    ; 57 ($39) = Floor Current, No Item, Only Used In Unseen Kingdom (no idea why 1 wasn't used)
                                fcb $25    ; 58 ($3A) = Floor Black?
                                fcb $6B    ; 59 ($3B) = Floor Current, No Item, Wall Horizontal
                                fcb $6B    ; 60 ($3C) = Floor Current, No Item, Wall Vertical
                                fcb $6B    ; 61 ($3D) = Floor Current, No Item, Wall Top Left Corner
                                fcb $6B    ; 62 ($3E) = Floor Current, No Item, Wall Top Right Corner
                                fcb $6B    ; 63 ($3F) = Floor Current, No Item, Wall Bottom Left Corner
                                fcb $6B    ; 64 ($40) = Floor Current, No Item, Wall Bottom Right Corner
                                fcb $6B    ; 65 ($41) = Floor Current, No Item, Wall Down T
                                fcb $6B    ; 66 ($42) = Floor Current, No Item, Wall Up T
                                fcb $6B    ; 67 ($43) = Floor Current, No Item, Wall Right T
                                fcb $6B    ; 68 ($44) = Floor Current, No Item, Wall Left T
                                fcb $6B    ; 69 ($45) = Floor Current, No Item, Wall Plus
                                fcb $69    ; 70 ($46) = Floor Current, Gem Item
                                fcb $6A    ; 71 ($47) = Floor Current, Paint Item
                                fcb $67    ; 72 ($48) = Floor Current, Wing Item
                                fcb $64    ; 73 ($49) = Floor Current, Key Item
                                fcb $65    ; 74 ($4A) = Floor Current, Clock Item
                                fcb $66    ; 75 ($4B) = Floor Current, Soda Item
                                fcb $68    ; 76 ($4C) = Floor Current, Hammer Item
                                fcb $41    ; 77 ($4D) = Brown Stone Wall
                                fcb $48    ; 78 ($4E) = Green Skull
                                fcb $4B    ; 79 ($4F) = Yellow Pillar
                                fcb $4C    ; 80 ($50) = Floor (?) Brown 4 Spheres
                                fcb $4E    ; 81 ($51) = Brown Ball, Black Outer
                                fcb $42    ; 82 ($52) = Door Vertical Stone
                                fcb $43    ; 83 ($53) = Door Horizontal Stone
                                fcb $4A    ; 84 ($54) = Door Vertical Green
                                fcb $49    ; 85 ($55) = Door Horizontal Green
                                fcb $6E    ; 86 ($56) = Cloud 3
                                fcb $6F    ; 87 ($57) = Cloud 4
                                fcb $70    ; 88 ($58) = Cloud 5
                                fcb $71    ; 89 ($59) = Cloud 6
                                fcb $72    ; 90 ($5A) = Cloud 7
                                fcb $73    ; 91 ($5B) = Cloud 8
                                fcb $74    ; 92 ($5C) = Cloud 9
                                fcb $75    ; 93 ($5D) = Cloud 10
                                fcb $6C    ; 94 ($5E) = Cloud 1
                                fcb $6D    ; 95 ($5F) = Cloud 2
                                fcb $76    ; 96 ($60) = Cloud 11
                                fcb $77    ; 97 ($61) = Cloud 12

levelToFloorTileLookup          fcb $05     ; 0
                                fcb $28     ; 1
                                fcb $05     ; 2
                                fcb $4D     ; 3
                                fcb $4F     ; 4
                                fcb $3F     ; 5
                                fcb $3C     ; 6
                                fcb $40     ; 7
                                fcb $3D     ; 8
                                fcb $40     ; 9
                                fcb $47     ; 10
                                fcb $4D     ; 11
                                fcb $3F     ; 12
                                fcb $4B     ; 13
                                fcb $4C     ; 14
                                fcb $05     ; 15
                                fcb $3D     ; 16
                                fcb $45     ; 17
                                fcb $3F     ; 18
                                fcb $40     ; 19
                                fcb $4C     ; 20
                                fcb $4B     ; 21
                                fcb $4F     ; 22
                                fcb $05     ; 23
                                fcb $40     ; 24
                                fcb $45     ; 25
                                fcb $3C     ; 26
                                fcb $3F     ; 27
                                fcb $05     ; 28
                                fcb $28     ; 29
                                fcb $45     ; 30
                                fcb $3D     ; 31

mmu                             fcb $38,$39,$3A,$3B,$3C,$3D,$3E,$3F
currentMap                      rmb 512
debugMessage                    fcn "Debug message\r"

;***********************
@noDiskSupport                  fcn "Disk support not detected\r"
@dataLoadError                  fcn "Error loading data\r"
@version                        fcn "v1.3.1\r"
@romsInStartingTakeover         fcn "Roms in, starting takeover\r"
@romsOutAboutToCallTests        fcn "Roms Out, about to call tests\r"
@romsOutDoneWithTests           fcn "Roms Out, done with tests\r"
@romsInDoneWithTakeover         fcn "Roms In, done with takeover\r"
start:
;***********************
    jsr     InitializeParameters
    jsr     InitializeDiskIo
    bcc     @initializeDiskIoSuccess
    leax    @noDiskSupport,pcr
    lbsr    PrintString
    lbra    @done
@initializeDiskIoSuccess
    ldmd    #1
    lbsr    SetFastSpeed
    lbsr    Set80x24TextMode
    leax    @version,pcr
    lbsr    PrintString
    leax    @romsInStartingTakeover,pcr
    lbsr    PrintString
    ;
    orcc    #$50
    lbsr    DisableInterruptSources
    lbsr    SwapRomsOut
    lda     #LOGICAL_6000_7FFF
    lbsr    SetSafeMmuScratchBlock
    andcc   #$AF
    leax    @romsOutAboutToCallTests,pcr
    lbsr    PrintString
    ;
    tst     shouldSkipLoadingData,PCR
    bne     @loadTileDataSuccess
    ;
    lbsr    LoadTileData
    bcc     @loadTileDataSuccess
    leax    @dataLoadError,pcr
    lbsr    PrintString
    bra     @doneWithTest
@loadTileDataSuccess
    lbsr    PerformScrollTest
    leax    @romsOutDoneWithTests,pcr
    lbsr    PrintString
    ;
@doneWithTest
    orcc    #$50
    lbsr    SwapRomsIn
    lbsr    RestoreInterruptSources
    lda     #LOGICAL_4000_5FFF
    lbsr    SetSafeMmuScratchBlock
    andcc   #$AF
    lbsr    Set80x24TextMode
    leax    @romsInDoneWithTakeover,pcr
    lbsr    PrintString
@done
    lbsr    ResetRgbPalette
    lbsr    SetSlowSpeed
    ldmd    #0
    rts

;***********************
InitializeParameters
;***********************
    pshs    b,a
    clr     printChar80x24_CurrentCursorX,pcr
    clr     printChar80x24_CurrentCursorY,pcr
    ldd     #$0000              ; $0000 = b00000000_00000000_000 = Physical address $00000
    std     verticalOffsetOnScreen,pcr
    ldd     #$1000              ; $1000 = b00010000_00000000_000 = Physical address $08000
    std     verticalOffsetOffScreen,pcr
    lda     #VIDEO_BUFFER_1_PHYSICAL_BLOCK
    sta     videoBufferPhysicalBlockOnScreen,pcr
    lda     #VIDEO_BUFFER_2_PHYSICAL_BLOCK
    sta     videoBufferPhysicalBlockOffScreen,pcr
    jsr     GetCurrentBasicCharacter
    cmpa    #':
    bne     @done
@ParameterLoop
    jsr     GetNextBasicCharacter
    beq     @done               ; Next character is 0, so we hit the end of the line.
    bsr     ProcessParameter
    bra     @ParameterLoop
@done
    puls    a,b,pc

;***********************
ProcessParameter
; ```plaintext
; > a = parameter character
; ```
;***********************
    cmpa    #'X                 ; X = skip data
    bne     @done
    sta     shouldSkipLoadingData,pcr
@done
    rts

;***********************
Set80x24TextMode
;***********************
    pshs    a
    ;         Video Mode
    ;         ┌────────── Bit  0   - BP   (Bitplane, 0=Text mode, 1=Graphics mode)
    ;         │┌───────── Bits 6   - Unused
    ;         ││┌──────── Bits 5   - BPI  (Composite color phase invert)
    ;         │││┌─────── Bits 4   - MOCH (Monochrome on composite video out)
    ;         ││││┌────── Bits 3   - H50  (1=50Hz video, 0=60Hz video)
    ;         │││││┌┬┬─── Bits 2-0 - LPR  (00x=1,010=2,011=8,100=9,101=10,110=11,111=infinite lines per row)
    lda     #%00000011
    sta     GIME_VideoMode_FF98
    ;
    ;         Video Resolution
    ;         ┌────────── Bit  7   - Unused
    ;         │┌┬──────── Bits 6-5 - LPF  (00=192, 01=200, 10=zero/infinate, 11=225 lines on screen)
    ;         │││┌┬┬───── Bits 4-2 - HRES (000=16,20,32,40,64,80,128,111=160 bytes per row)
    ;         ││││││┌┬─── Bits 1-0 - CRES (00=2, 01=4, 10=16, 11=undefined colors)
    lda     #%00010101
    sta     GIME_VideoResolution_FF99
    ;
    ;         Border Color
    ;         ┌┬───────── Bits 7-6 - Unused
    ;         ││┌──┬───── Bits 5,2 - Red
    ;         │││┌─│┬──── Bits 4,1 - Green
    ;         ││││┌││┬─── Bits 3,0 - Blue
    lda     #%00010010
    sta     GIME_BorderColor_FF9A
    ;
    ;         Vertical Scroll
    ;         ┌┬┬┬─────── Bits 7-4 - Unused
    ;         ││││┌┬┬┬─── Bits 3-0 - VSC (Vertical Scroll Register for smooth scrolling in text modes)
    lda     #%00000000
    sta     GIME_VerticalScroll_FF9C
    ;
    ;         Vertical Offset 1
    ;         ┌┬┬┬┬┬┬┬─── Bits 7-0 - Bits 18-11 of 19 bit Physical address for start of display
    lda     #%11011000
    sta     GIME_VerticalOffset1_FF9D
    ;
    ;         Vertical Offset 0
    ;         ┌┬┬┬┬┬┬┬─── Bits 7-0 - Bits 10-3 of 19 bit Physical address for start of display (bits 2-0 are always 0)
    lda     #%00000000  ; %11011000_00000000_000 = &h6C000
    sta     GIME_VerticalOffset0_FF9E
    ;
    ;         Horizontal Offset
    ;         ┌────────── Bit  7   - HVEN - Horzontal Virtual Screen Enable
    ;         │┌┬┬┬┬┬┬─── Bits 6-0 - Horizontal Offset. Value * 2 is added to address at $FF9D/$FF9E if HVEN is set
    lda     #%00000000
    sta     GIME_HorizontalOffset_FF9F
    ;
    ;         Initialize Register 0
    ;         ┌────────── Bit  0   - CoCo 3 mode if set
    ;         │┌───────── Bit  6   - MMU Enabled
    ;         ││┌──────── Bit  5   - GIME IRQ disabled
    ;         │││┌─────── Bit  4   - GIME FIRQ disabled
    ;         ││││┌────── Bit  3   - Vector RAM at $FExx enabled if set
    ;         │││││┌───── Bit  2   - Standard SCS (DISK) normal if set
    ;         ││││││┌┬─── Bits 1-0 - ROM Map (0x=16K internal/16K external,10=32K internal, 11=32K external (except interrupt vectors))
    lda     #%01001110
    sta     gimeRegister0Value
    sta     GIME_InitializeRegister0_FF90
    ;
    puls    a,pc

;***********************
Set320x200x16_GraphicsMode
;***********************
    pshs    a
    ;         VideoMode
    ;         ┌────────── Bit  0   - BP   (Bitplane, 0=Text mode, 1=Graphics mode)
    ;         │┌───────── Bits 6   - Unused
    ;         ││┌──────── Bits 5   - BPI  (Composite color phase invert)
    ;         │││┌─────── Bits 4   - MOCH (Monochrome on composite video out)
    ;         ││││┌────── Bits 3   - H50  (1=50Hz video, 0=60Hz video)
    ;         │││││┌┬┬─── Bits 2-0 - LPR  (00x=1,010=2,011=8,100=9,101=10,110=11,111=infinite lines per row)
    lda     #%10000001
    sta     GIME_VideoMode_FF98
    ;
    ;         Video Resolution
    ;         ┌────────── Bit  7   - Unused
    ;         │┌┬──────── Bits 6-5 - LPF  (00=192, 01=200, 10=zero/infinate, 11=225 lines on screen)
    ;         │││┌┬┬───── Bits 4-2 - HRES (000=16,20,32,40,64,80,128,111=160 bytes per row)
    ;         ││││││┌┬─── Bits 1-0 - CRES (00=2, 01=4, 10=16, 11=undefined colors)
    lda     #%00111110
    sta     GIME_VideoResolution_FF99
    ;
    ;         Border Color
    ;         ┌┬───────── Bits 7-6 - Unused
    ;         ││┌──┬───── Bits 5,2 - Red
    ;         │││┌─│┬──── Bits 4,1 - Green
    ;         ││││┌││┬─── Bits 3,0 - Blue
    lda     #%00000000
    sta     GIME_BorderColor_FF9A
    ;
    ;         Vertical Scroll
    ;         ┌┬┬┬─────── Bits 7-4 - Unused
    ;         ││││┌┬┬┬─── Bits 3-0 - VSC (Vertical Scroll Register for smooth scrolling in text modes)
    lda     #%00000000
    sta     GIME_VerticalScroll_FF9C
    ;
    ;         Vertical Offset 1
    ;         ┌┬┬┬┬┬┬┬─── Bits 7-0 - Bits 18-11 of 19 bit Physical address for start of display
    lda     #%00000000
    sta     GIME_VerticalOffset1_FF9D
    ;
    ;         Vertical Offset 0
    ;         ┌┬┬┬┬┬┬┬─── Bits 7-0 - Bits 10-3 of 19 bit Physical address for start of display (bits 2-0 are always 0)
    lda     #%00000000  ; %00000000_00000000_000 = %000_0000_0000_0000_0000 = &h00000
    ;       #%00010000  ; %00011100_00000000_000 = %000_1000_0000_0000_0000 = &h0E000
    sta     GIME_VerticalOffset0_FF9E
    ;
    ;         Horizontal Offset
    ;         ┌────────── Bit  7   - HVEN - Horzontal Virtual Screen: 1 = Enabled, 0 = Disabled
    ;         │┌┬┬┬┬┬┬─── Bits 6-0 - Horizontal Offset. Value * 2 is added to address at $FF9D/$FF9E if HVEN is set
    lda     #%00000000
    sta     GIME_HorizontalOffset_FF9F
    ;
    ;         Initialize Register 0
    ;         ┌────────── Bit  0   - 0 = CoCo 3, 1 = CoCo 1/2
    ;         │┌───────── Bit  6   - MMU Enable
    ;         ││┌──────── Bit  5   - GIME IRQ enable
    ;         │││┌─────── Bit  4   - GIME FIRQ enable
    ;         ││││┌────── Bit  3   - Vector RAM at $FExx enabled if set
    ;         │││││┌───── Bit  2   - Standard SCS (DISK) normal if set
    ;         ││││││┌┬─── Bits 1-0 - ROM Map (0x=16K internal/16K external,10=32K internal, 11=32K external (except interrupt vectors))
    lda     #%01001110
    sta     gimeRegister0Value
    sta     GIME_InitializeRegister0_FF90
    ;
    puls    a,pc

;***********************
Clear320x200x16ScreenBuffers:
;***********************
    pshs    x,b,a
    pshsw
    ;
    lda     #VIDEO_BUFFER_1_PHYSICAL_BLOCK
    ldb     #LOGICAL_6000_7FFF
    lbsr    MapPhysicalBlockToLogicalBlock
    pshs    a
    ldb     #8                              ; 160(bytes)x200(lines)x2(buffers) = 64000 = 8 blocks
    clra
    pshs    a
    ;
@clearBlock
    ldx     #$6000
    ldw     #$2000
    tfm     s,x+
    ;
    inc     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_6000_7FFF
    decb
    bne     @clearBlock
    ;
@doneClear
    puls    a
    puls    a
    ldb     #LOGICAL_6000_7FFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ;
    pulsw
    puls    a,b,x,pc

;***********************
DisableInterruptSources
;***********************
    lda     PIA0SideAControlRegister_FF01
    ;ora     #$01                            ; Enable HSYNC IRQ
    anda    #$FE                            ; Disable HSYNC IRQ
    sta     PIA0SideAControlRegister_FF01

    lda     PIA0SideBControlRegister_FF03
    ora     #$01
    sta     PIA0SideBControlRegister_FF03   ; Enable VSYNC IRQ

    lda     PIA1SideAControlRegister_FF21
    anda    #$FE
    sta     PIA1SideAControlRegister_FF21   ; Disable Cassette Data FIRQ

    lda     PIA1SideBControlRegister_FF23
    anda    #$FE
    sta     PIA1SideBControlRegister_FF23   ; Disable Cartridge FIRQ

    lda     #%01001110
    sta     gimeRegister0Value
    sta     GIME_InitializeRegister0_FF90   ; 0  = CoCo 3 Mode, 1  = CoCo 1/2 Compatible, 
                                            ; 1  = MMU enabled (0 = disabled)
                                            ; 0  = GIME IRQ disabled (1 = enabled)
                                            ; 0  = GIME FIRQ disabled (1 = enabled)
                                            ; 1  = Vector RAM at $FEXX enabled (0 = disabled)
                                            ; 1  = Standard SCS (DISK) Normal (0 = expand)
                                            ; 10 = ROM Map 32k Internal
                                            ;      0x = 16K Internal, 16K External
                                            ;      11 = 32K External - Except Interrupt Vectors

    lda     #%00000000
    sta     GIME_InterruptReqEnable_FF92        ; Disable IRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    sta     GIME_FastInterruptReqEnable_FF93    ; Disable FIRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    rts

;***********************
RestoreInterruptSources
;***********************
    lda     PIA0SideBControlRegister_FF03
    ora     #$01
    sta     PIA0SideBControlRegister_FF03   ; Enable VSYNC IRQ

    lda     PIA0SideAControlRegister_FF01
    anda    #$FE
    sta     PIA0SideAControlRegister_FF01   ; Disable HSYNC IRQ

    lda     PIA1SideAControlRegister_FF21
    anda    #$FE
    sta     PIA1SideAControlRegister_FF21   ; Disable the Cassette Data FIRQ

    lda     PIA1SideBControlRegister_FF23
    anda    #$FE
    sta     PIA1SideBControlRegister_FF23   ; Disable the Cartridge FIRQ

    lda     #%01001110
    sta     GIME_InitializeRegister0_FF90   ; 0  = CoCo 3 Mode, 1  = CoCo 1/2 Compatible, 
                                            ; 1  = MMU enabled (0 = disabled)
                                            ; 0  = GIME IRQ disabled (1 = enabled)
                                            ; 0  = GIME FIRQ disabled (1 = enabled)
                                            ; 1  = Vector RAM at $FEXX enabled (0 = disabled)
                                            ; 1  = Standard SCS (DISK) Normal (0 = expand)
                                            ; 10 = ROM Map 32k Internal
                                            ;      0x = 16K Internal, 16K External
                                            ;      11 = 32K External - Except Interrupt Vectors

    lda     #%00000000
    sta     GIME_InterruptReqEnable_FF92        ; Disable IRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    sta     GIME_FastInterruptReqEnable_FF93    ; Disable FIRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    rts

;***********************
romInStack      rmb 2
romOutStack     fdb $600
SwapRomsOut
;***********************
    pshs    x,b,a
    ;
    sta     RamMode_FFDF                ; Switch $8xxx-$Fxxx RAM mode
    ;
    ldb     #PHYSICAL_060000_061FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_0000_1FFF
    leax    mmu+LOGICAL_0000_1FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_068000_069FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_8000_9FFF
    leax    mmu+LOGICAL_8000_9FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_06A000_06BFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_A000_BFFF
    leax    mmu+LOGICAL_A000_BFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_06C000_06DFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_C000_DFFF
    leax    mmu+LOGICAL_C000_DFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_06E000_06FFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_E000_FFFF
    leax    mmu+LOGICAL_E000_FFFF,pcr
    stb     ,x
    ;
    ldb     #LOGICAL_8000_9FFF
    stb     safeMmuLogicalBlock,pcr
    ldx     #$8000
    stx     safeMmuLogicalBlockAddress,pcr
    ;
    lbsr    SetupInterruptVectors
    ;
    ;                           ; $7F50 = old_a, $7F51 = old_b, $7F52-3 = old_x, $7F54-5 = return
    tfr     s,x                 ; s = $7F50, x = $7F50
    leas    6,s                 ; s = $7F56
    sts     romInStack,pcr      ; romInStack = $7F56
    lds     romOutStack,pcr     ; s = $0600
    ldd     4,x                 ; d = return
    pshs    b,a                 ; $05FE-F = return, s = $05FE
    ldd     ,x                  ; d = old_d
    ldx     2,x                 ; x = old_x
    ;
    ldb     #PHYSICAL_066000_067FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_6000_7FFF
    leax    mmu+LOGICAL_6000_7FFF,pcr
    stb     ,x
    ;
    rts

;***********************
SwapRomsIn
;***********************
    pshs    x,b,a
    ;
    lbsr    RestoreInterruptVectors
    ;
    ldb     #PHYSICAL_076000_077FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_6000_7FFF
    leax    mmu+LOGICAL_6000_7FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_078000_079FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_8000_9FFF
    leax    mmu+LOGICAL_8000_9FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_07A000_07BFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_A000_BFFF
    leax    mmu+LOGICAL_A000_BFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_07C000_07DFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_C000_DFFF
    leax    mmu+LOGICAL_C000_DFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_07E000_07FFFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_E000_FFFF
    leax    mmu+LOGICAL_E000_FFFF,pcr
    stb     ,x
    ;
    lda     LOGICAL_4000_5FFF
    lbsr    SetSafeMmuScratchBlock
    ;
    ;                           ; $05FA = old_a, $05FB = old_b, $05FC-D = old_x, $05FE-F = return
    tfr     s,x                 ; s = $05FA, x = $05FA
    leas    6,s                 ; s = $0600
    sts     romOutStack,pcr     ; romOutStack = $0600
    lds     romInStack,pcr      ; s = $7F56
    ldd     4,x                 ; d = return
    pshs    b,a                 ; $7F54-5 = return, s = $7F54
    ldd     ,x                  ; x = old_x
    ldx     2,x                 ; d = old_d
    ;
    pshs    x,b
    ldb     #PHYSICAL_070000_071FFF
    stb     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_0000_1FFF
    leax    mmu+LOGICAL_0000_1FFF,pcr
    stb     ,x
    puls    b,x
    ;
    lbsr    UpdateRomCursorPosition
    ;
    rts

;***********************
PrintHexWord
; ```plaintext
; > d = number to print as four hex digits
; ```
    pshs    a
    lbsr    PrintHexByte
    tfr     b,a
    lbsr    PrintHexByte
    puls    a,pc

;***********************
@buffer     rmb 3
PrintHexByte
; ```plaintext
; > a = number to print as two hex digits
; ```
    pshs    x,b,a
    leax    @buffer,pcr
    tfr     a,b
    lsra
    lsra
    lsra
    lsra
    andb    #$0F
    addd    #'0*256+'0
    cmpa    #'9
    bls     @doneFirstDigit
    adda    #7
@doneFirstDigit
    cmpb    #'9
    bls     @doneSecondDigit
    addb    #7
@doneSecondDigit
    std     ,x++
    clr     ,x
    leax    -2,x
    ;
    bsr     PrintString
    puls    a,b,x,pc

;***********************
PrintString
; ```plaintext
; > x = pointer to string to print (zero terminated)
; ```
    pshs    x,a
@next
    lda     ,x+
    beq     @done
    bsr     PrintChar80x24
    bra     @next
@done
    puls    a,x,pc

;***********************
PrintChar80x24
; ```plaintext
; > a = Character to print
; ```
    pshs    x,b,a
    pshsw
    tfr     a,e
    ; Map in $6C000 to our safeMmuLogicalBlock
    lda     #PHYSICAL_06C000_06DFFF
    lbsr    MapPhysicalBlockToSafeMmuLogicalBlock
    pshs    a
    ;
    ldx     safeMmuLogicalBlockAddress,pcr
    lda     printChar80x24_CurrentCursorY,pcr
@adjustForY
    tsta
    beq     @doneAdjustForY
    leax    160,x
    deca
    bra     @adjustForY
@doneAdjustForY
    ldb     printChar80x24_CurrentCursorX,pcr
    abx
    abx
    ; Print the character
    cmpe    #13                         ; Newline?
    bne     @not_newline
    ; Print a newline (blank rest of line, move cursor down one and all the way to the left)
    lda     printChar80x24_CurrentCursorX,pcr
    ldb     #32                         ; space
@blankRestOfLine
    tsta
    beq     @doneBlankingLine
    stb     ,x+
    clr     ,x+
    deca
    bra     @blankRestOfLine
@doneBlankingLine
    inc     printChar80x24_CurrentCursorY,pcr ; Move down one line
    clr     printChar80x24_CurrentCursorX,pcr ; Move back to beginning of line
    bra     @donePrint
    ; Print the character
@not_newline
    ste     ,x+
    clr     ,x+
    inc     printChar80x24_CurrentCursorX,pcr ; Move one character to the right
    ;
@donePrint
    lda     printChar80x24_CurrentCursorX,pcr
    cmpa    #80                               ; Did we just print in the right-most column?
    blt     @doneAdjustX
    inc     printChar80x24_CurrentCursorY,pcr ; Move down one line
    clr     printChar80x24_CurrentCursorX,pcr ; Move back to beginning of line
@doneAdjustX
    lda     printChar80x24_CurrentCursorY,pcr
    cmpa    #24                               ; Did we just move off the bottom of the screen?
    blt     @doneAdjustY
    clr     printChar80x24_CurrentCursorY,pcr ; Move back to the top of the screen
@doneAdjustY
    ; Map the original physical block back into our safeMmuLogicalBlock
    puls    a
    lbsr    MapPhysicalBlockToSafeMmuLogicalBlock
    ; Character printed.  We're out'a here!
    pulsw
    puls    a,b,x,pc

;***********************
UpdateRomCursorPosition:
;***********************
    pshs    b,a
    ldd     printChar80x24_CurrentCursorX   ;// Load cursor x into a and cursor y into b
    incb                                    ;// cursor y is 1 based for ROMS, 0 based for us
    std     $FE02                           ;// Store cursor x at $FE02 and cursor y at $FE03
    pshs    a                               ;// Save cusor x
    lda     #160
    mul                                     ;// d = 160 * cursor y
    addb    ,s+                             ;// d += cursor x
    adca    #0
    addd    #$2000                          ;// d += $2000
    std     $FE00                           ;// Store address of cursor at $FE00
    puls    a,b,pc

pcbloxFilename      fcn 'PCBLOX  DAT'
ckmapFilename       fcn 'CKMAP   DTA'
loadingMessage      fcn 'Loading '
endLoadingMessage   fcb 13,0
;***********************
LoadTileData:
;***********************
    pshs    x,b,a
    orcc    #$50
    lbsr    SwapRomsIn
    andcc   #$AF
    ;

    lda     #LOGICAL_4000_5FFF
    lbsr    SetSafeMmuScratchBlock
    pshs    a
    ;
    leax    loadingMessage,pcr
    lbsr    PrintString
    leax    PrintProgressIndicator,pcr
    stx     progressCallback
    ; Load PCBLOX.DAT
    leax    pcbloxFilename,pcr
    stx     addressOfFilename,pcr
    lda     #TILE_DATA_PHYSICAL_BLOCK
    sta     physicalBlock,pcr
    clrd
    std     physicalOffset,pcr
    lbsr    LoadFileStartingAtPhysicalBlock
    bcc     @success
@error
    leax    endLoadingMessage,pcr
    lbsr    PrintString
    puls    a
    lbsr    MapPhysicalBlockToSafeMmuLogicalBlock
    ;
    orcc    #$50
    lbsr    SwapRomsOut
    andcc   #$AF
    coma
    bra     @done
    ;
@success
    leax    endLoadingMessage,pcr
    lbsr    PrintString
    puls    a
    lbsr    MapPhysicalBlockToSafeMmuLogicalBlock
    ;
    orcc    #$50
    lbsr    SwapRomsOut
    andcc   #$AF
    clra
@done
    puls    a,b,x,pc

progressIndicator   fcn '.'
;***********************
PrintProgressIndicator:
;***********************
    pshs    x
    leax    progressIndicator,pcr
    lbsr    PrintString
    puls    x,pc

shouldScrollUp              fcb     0
shouldScrollDown            fcb     0
shouldScrollLeft            fcb     0
shouldScrollRight           fcb     0
shouldQuit                  fcb     0
shouldCycleToNextLevel      fcb     0
shouldCycleToPreviousLevel  fcb     0
playerHorizontalSpeed       fcb     0
playerVerticalSpeed         fcb     0
;***********************
PerformScrollTest:
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    lbsr    Clear320x200x16ScreenBuffers
    lbsr    SavePalette
    lbsr    SetEgaPalette
    lbsr    Set320x200x16_GraphicsMode
    ;
    lda     $FF01
    anda    #$FE
    sta     $FF01
    ;
    lda     #0
@levelLoop
    pshs    a
    lbsr    InitializeLevel
    lbsr    PrerenderLevel
    lbsr    BlitLevelToOffScreen
    ;lbsr    DrawTilesOffScreen
    ;
    * pulsw
    * puls    a,b,x,y,pc
    ;
    lbsr    WaitForVsync
    lbsr    SwapOnScreenAndOffScreenBuffers
    ldy     #$A000-2
    clr     vsyncCount
@startLoop
    lda     #%00100000      ; BorderColor      - Unused=(7-6), R1(5), G1(4), B1(3), R0(2), G0(1), B0(0)
    sta     GIME_BorderColor_FF9A
    ;
    lbsr    ProcessGameKeys
    ;
    tst     playerHorizontalSpeed,pcr
    beq     @doneApplyingHorizontalFriction
    blt     @applyPositiveHorizontalFriction
    dec     playerHorizontalSpeed,pcr
    bra     @doneApplyingHorizontalFriction
@applyPositiveHorizontalFriction
    inc     playerHorizontalSpeed,pcr
@doneApplyingHorizontalFriction
    tst     playerVerticalSpeed,pcr
    beq     @doneApplyingVerticalFriction
    blt     @applyPositiveVerticalFriction
    dec     playerVerticalSpeed,pcr
    bra     @doneApplyingVerticalFriction
@applyPositiveVerticalFriction
    inc     playerVerticalSpeed,pcr
@doneApplyingVerticalFriction
@checkAccelerateRight
    tst     shouldScrollRight,pcr
    beq     @checkAccelerateLeft
    lda     playerHorizontalSpeed,pcr
    adda    #2
    cmpa    #8
    ble     @doneUpperCapHorizontalSpeed
    lda     #8
@doneUpperCapHorizontalSpeed
    sta     playerHorizontalSpeed,pcr
@checkAccelerateLeft
    tst     shouldScrollLeft,pcr
    beq     @checkAccelerateDown
    lda     playerHorizontalSpeed,pcr
    suba    #2
    cmpa    #-8
    bge     @doneLowerCapHorizontalSpeed
    lda     #-8
@doneLowerCapHorizontalSpeed
    sta     playerHorizontalSpeed,pcr
@checkAccelerateDown
    tst     shouldScrollDown,pcr
    beq     @checkAccelerateUp
    lda     playerVerticalSpeed,pcr
    adda    #2
    cmpa    #8
    ble     @doneUpperCapVerticalSpeed
    lda     #8
@doneUpperCapVerticalSpeed
    sta     playerVerticalSpeed,pcr
@checkAccelerateUp
    tst     shouldScrollUp,pcr
    beq     @doneCheckAccelerate
    lda     playerVerticalSpeed,pcr
    suba    #2
    cmpa    #-8
    bge     @doneLowerCapVerticalSpeed
    lda     #-8
@doneLowerCapVerticalSpeed
    sta     playerVerticalSpeed,pcr
@doneCheckAccelerate
    ;
    lda     playerHorizontalSpeed,pcr
    ldx     <playfieldTopLeftX
    leax    a,x
    cmpx    #0
    bge     @doneLowerCapPlayfieldX
    ldx     #0
    clr     playerHorizontalSpeed,pcr
@doneLowerCapPlayfieldX
    cmpx    #1024-320
    ble     @doneUpperCapPlayfieldX
    ldx     #1024-320
    clr     playerHorizontalSpeed,pcr
@doneUpperCapPlayfieldX
    stx     <playfieldTopLeftX
    ;
    lda     playerVerticalSpeed,pcr
    ldx     <playfieldTopLeftY
    leax    a,x
    cmpx    #0
    bge     @doneLowerCapPlayfieldY
    ldx     #0
    clr     playerVerticalSpeed,pcr
@doneLowerCapPlayfieldY
    cmpx    #512-144-1
    ble     @doneUpperCapPlayfieldY
    ldx     #512-144-1
    clr     playerVerticalSpeed,pcr
@doneUpperCapPlayfieldY
    stx     <playfieldTopLeftY
    lbsr    UpdateScreen
    lbsr    SwapOnScreenAndOffScreenBuffers
@doneScroll
    ;
    lda     #%00000001      ; BorderColor      - Unused=(7-6), R1(5), G1(4), B1(3), R0(2), G0(1), B0(0)
    sta     GIME_BorderColor_FF9A
    ;
    lbsr    WaitForVsync
    ; vvvvv DEBUG vvvvv
    ; lda     vsyncCount
    ; lbsr    PrintHexByte
    ; clr     vsyncCount
    ; ^^^^^ DEBUG ^^^^^
    tst     shouldQuit,pcr
    bne     @quitGame
@checkCycleNextLevel
    tst     shouldCycleToNextLevel,pcr
    beq     @checkCyclePreviousLevel
    puls    a
    inca
    anda    #$0F
    lbra    @levelLoop
@checkCyclePreviousLevel
    tst     shouldCycleToPreviousLevel,pcr
    beq     @doneCheckingKeys
    puls    a
    deca
    anda    #$0F
    lbra    @levelLoop
@doneCheckingKeys
    lbra    @startLoop
    ;
@quitGame
    puls    a
    lbsr    ClearGameKeys
    lbsr    RestorePalette
    ;
@done
    pulsw
    puls    a,b,x,y,pc

;***********************
UpdateScreen
;***********************
    ;
    lbsr    BlitLevelToOffScreen
    ; If PFY > PFY_OffScreen
    ;     Draw PFY-PFY_OffScreen lines
    ;     From PFX,PFY+144-PFTLY_OffScreen+PFTLY lines to PFX+319,PFY+143 on Prerendered Level
    ;     To verticalOffsetOffScreen+256* {FINISH THIS}
    rts

;***********************
ProcessGameKeys
;***********************
    lbsr    ClearGameKeys
    leax    keyColumnScans,pcr
    ldb     #1
@checkUpArrow
    lda     3,x
    anda    #%00001000
    beq     @checkDownArrow
    stb     shouldScrollUp,pcr
@checkDownArrow
    lda     4,x
    anda    #%00001000
    beq     @checkLeftArrow
    stb     shouldScrollDown,pcr
@checkLeftArrow
    lda     5,x
    anda    #%00001000
    beq     @checkRightArrow
    stb     shouldScrollLeft,pcr
@checkRightArrow
    lda     6,x
    anda    #%00001000
    beq     @checkSpacebar
    stb     shouldScrollRight,pcr
@checkSpacebar
    lda     7,x
    anda    #%00001000
    beq     @checkN
    stb     shouldQuit,pcr
@checkN
    lda     6,x
    anda    #%00000010
    beq     @checkP
    stb     shouldCycleToNextLevel,pcr
@checkP
    lda     0,x
    anda    #%00000100
    beq     @doneCheckingKeys
    stb     shouldCycleToPreviousLevel,pcr
@doneCheckingKeys
    rts

;***********************
ClearGameKeys
;***********************
    pshs    a
    clra
    sta     shouldScrollUp,pcr
    sta     shouldScrollDown,pcr
    sta     shouldScrollLeft,pcr
    sta     shouldScrollRight,pcr
    sta     shouldQuit,pcr
    sta     shouldCycleToNextLevel,pcr
    sta     shouldCycleToPreviousLevel,pcr
    puls    a,pc

;***********************
SetEgaPalette:
;***********************
    pshs    y,x,b,a
    ;
    ; Store the current pallete values for later restoring
    ldy     #ColorPaletteFirstRegister_FFB0
    leax    paletteStore,pcr
    ldb     #15
@loop_store
    lda     b,y
    sta     b,x
    tstb
    beq     @done_store
    decb
    bra     @loop_store
@done_store
    ;
    ; Set the pallete
    leax    paletteCga,pcr
    ldb     #15
@loop_set
    lda     b,x
    sta     b,y
    tstb
    beq     @done_set
    decb
    bra     @loop_set
@done_set
    ;
    puls    a,b,x,y,pc

;***********************
SavePalette:
;***********************
    pshs    y,x,b,a
    ;
    ldy     #ColorPaletteFirstRegister_FFB0
    leax    paletteStore,pcr
    ldb     #15
@loop
    lda     b,y
    sta     b,x
    tstb
    beq     @done
    decb
    bra     @loop
@done
    ;
    puls    a,b,x,y,pc

;***********************
RestorePalette:
;***********************
    pshs    y,x,b,a
    ;
    ldy     #ColorPaletteFirstRegister_FFB0
    leax    paletteStore,pcr
    ldb     #15
@loop
    lda     b,x
    sta     b,y
    tstb
    beq     @done
    decb
    bra     @loop
@done
    ;
    puls    a,b,x,y,pc

waitingForVsync     fcb     $00
vsyncCount          fcb     $00
;***********************
InterruptHandler:
;***********************
    lda     PIA0SideBControlRegister_FF03
    bpl     @done
    ;
    leax    keyColumnScans,pcr
    lda     #%11111111
    sta     PIA0SideBDataRegister_FF02  ; Disable all keyboard column strobes
    ;
    ldb     #%00000001      ; Start off reading column 0
@loop_keyscan
    comb
    stb     PIA0SideBDataRegister_FF02  ; Enable the next column
    lda     PIA0SideADataRegister_FF00  ; Read the column
    coma                    ; Invert bits so 1=key down, 0=key up
    sta     ,x+             ; Store in next keyColumnScans element
    ;
    comb
    lslb                    ; Next column. Shift bits in b to the left
    bne     @loop_keyscan
    ;
    inc     vsyncCount
    clr     waitingForVsync
    ;
    lda     PIA0SideBDataRegister_FF02  ; Acknowledge the VSYNC interrupt
@done
    rti

;***********************
WaitForKeyPressNoRom:
;***********************
    pshs    b,a
@waitForNoKey
    bsr     IsKeyDownNoRom
    bne     @waitForNoKey
@waitForKey
    bsr     IsKeyDownNoRom
    beq     @waitForKey
@waitForNoKeyAgain
    bsr     IsKeyDownNoRom
    bne     @waitForNoKeyAgain
    ;
    puls    a,b,pc

;***********************
IsKeyDownRom:
; ```plaintext
; < a = 1 if any key is down, 0 otherwise
; ```
    ; Make ROM call to see if key is pressed
    jsr     [$A000]
    cmpa    #0
    bhi     @keyPressed
    clra                            ; Return 0 for no key down
    bra     @done
@keyPressed
    lda     #1                      ; Return 1 for key down
@done
    rts

;***********************
IsKeyDownNoRom:
; ```plaintext
; < a = 1 if any key is down, 0 otherwise
; ```
;***********************
    pshs    x
    leax    keyColumnScans,pcr
    lda     #7
@checkScan
    tst     a,x
    bne     @keyDown
    tsta
    beq     @noKeyDown
    deca
    bra     @checkScan
@noKeyDown
    lda     #0                      ; Return 0 for no key down
    bra     @done
@keyDown
    lda     #1                      ; Return 1 for key down
@done
    puls    x,pc

;***********************
SetSafeMmuScratchBlock:
; ```plaintext
; > a = Logical MMU block to use as a safe scratch block for mapping
; ```
;***********************
    pshs    x,a
    orcc    #$50
    sta     safeMmuLogicalBlock,pcr
    ldx     #$0000
@loop
    tsta
    beq     @doneMultiply
    leax    $2000,x
    deca
    bra     @loop
@doneMultiply
    stx     safeMmuLogicalBlockAddress,pcr
    andcc   #$AF
    ;
    puls    a,x,pc

;***********************
MapPhysicalBlockToSafeMmuLogicalBlock:
; ```plaintext
; > a = physical block # to map
; < a = originally mapped physical block #
; ```
    pshs    b
    ;
    ldb     safeMmuLogicalBlock,pcr
    bsr     MapPhysicalBlockToLogicalBlock
    ;
    puls    b,pc

;***********************
MapPhysicalBlockToLogicalBlock:
; ```plaintext
; > a = physical block # to map
; > b = logical block # to map
; < a = originally mapped physical block #
; ```
    pshs    x,b
    ; Map the phsyical block in a to the logical block in b
    ldx     #MMU_BLOCK_REGISTERS_FIRST
    abx
    sta     ,x
    ; Get original physical block mapped to safeMmuLogicalBlock
    leax    mmu,pcr
    abx
    tfr     a,b
    lda     ,x
    stb     ,x
    ;
    puls    b,x,pc

;***********************
WaitForVsync:
;***********************
    inc     waitingForVsync,pcr
@wait
    tst     waitingForVsync,pcr
    bne     @wait
    rts

;***********************
SetFastSpeed:
;***********************
    clr     SetClockSpeedR1_FFD9
    rts

;***********************
SetSlowSpeed:
;***********************
    clr     ClearClockSpeedR1_FFD8
    rts

;***********************
ResetRgbPalette:
;***********************
    pshs    y,x,a
    ; Fix an error in SECB where it only set 15 of the 16 palette registers to the RGB colors
    lda     #16
    sta     $E649
    ; Call the SECB routine to set the paleete to the RGB colors
    jsr     $E5FA
    puls    a,x,y,pc

;***********************
originalVectors    rmb 18
SetupInterruptVectors:
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    ldx     #$FEEE
    leay    originalVectors,pcr
    ldw     #18
    tfm     x+,y+
    ;
    ldx     #$FEEE
    lda     #$7E            ;// jmp
    leay    InterruptHandler,pcr
    ldb     #6
@setVector
    sta     ,x+
    sty     ,x++
    decb
    bne     @setVector
    ;
    pulsw
    puls    a,b,x,y,pc

;***********************
RestoreInterruptVectors:
;***********************
    pshs    y,x
    pshsw
    leax    originalVectors,pcr
    ldy     #$FEEE
    ldw     #18
    tfm     x+,y+
    pulsw
    puls    x,y,pc

;***********************
InitializeLevel
; ```plaintext
; > a = Level #
; ```
    pshs    x,b,a
    orcc    #$50
    lbsr    SwapRomsIn
    andcc   #$AF
    ;
    ; Load MAP from CKMAP.DTA
    leax    ckmapFilename,pcr
    lbsr    OpenFileForRead
    bcs     @error
@loop
    ldx     #32*16
    stx     bytesToRead,pcr
    leax    currentMap,pcr
    stx     bytesReadBuffer,pcr
    lbsr    ReadBytesFromFileIntoBuffer
    bcs     @error
    tsta
    beq     @success
    deca
    bra     @loop
@error
    orcc    #$50
    lbsr    SwapRomsOut
    andcc   #$AF
    coma
    bra     @initializeFloorTiles
    ;
@success
    lbsr    CloseAllFiles
    orcc    #$50
    lbsr    SwapRomsOut
    andcc   #$AF
@initializeFloorTiles
    clra
    lda     #TILE_DATA_PHYSICAL_BLOCK+6
    ldb     #LOGICAL_A000_BFFF
    lbsr    MapPhysicalBlockToLogicalBlock  ; Map the Tile Data block containing the destination tiles
    lda     ,s                              ; Retrieve the map # again
    leax    levelToFloorTileLookup,pcr
    lda     a,x                             ; a = Tile # for floor tile for this map
    pshs    a
    ; Calculate the physical block the tile is in by taking the high nibble (4 bits)
    ; of the tile # and adding the first block # of the tile data.
    ; ex: Tile # $6B is in physical block $10 (first block of tile data) + $6, or $16
    lsra
    lsra
    lsra
    lsra
    adda    #TILE_DATA_PHYSICAL_BLOCK
    ldb     #LOGICAL_6000_7FFF
    lbsr    MapPhysicalBlockToLogicalBlock
    puls    a
    ; Calculate the offset into the tile #'s block where tile # starts by taking the
    ; low nibble of the tile # and multiplying by 512 (number of bytes in each tile).
    ; ex: Tile # $6B is at offset $B * 512, or $1600
    anda    #$0F
    lsla
    clrb                                    ; d now contains the tile # * 512
    ldx     #$6000
    leax    d,x                             ; x = address of 512 bytes for the floor tile to copy
    pshs    x
    ldy     #$A000+(512*4)                  ; y = address of first (of 8) 512 bytes to copy the floor tile to
    lda     #8
@nextTile
    ldw     #512
    tfm     x+,y+
    ldx     ,s
    deca
    bne     @nextTile
    puls    x
    ;
    ; Copy floor tile for level (look up in levelToFloorTileLookup) to the 8 tiles at indexes 100-107 ($64-$6B)
    ; Mask and copy the 7 item tiles (indexes 80-86) to the 8 tiles at indexes 100-107
    ; * See routine UpdateFloorAndItemBloxInBloxData in CK disassembly
    ;
@done
    clrd
    std     <playfieldTopLeftX
    std     <playfieldTopLeftY
    puls    a,b,x,pc

;***********************
BlitLevelToOffScreen2
;***********************
    pshs    y,x,b,a
    pshsw
    lda     videoBufferPhysicalBlockOnScreen,pcr
    ldb     #LOGICAL_6000_7FFF
    lbsr    MapPhysicalBlockToLogicalBlock
    pshs    a
    lda     videoBufferPhysicalBlockOffScreen,pcr
    incb
    lbsr    MapPhysicalBlockToLogicalBlock
    pshs    a
    ldw     #$2000
    ldx     #$6000
    ldy     #$A000
    tfm     x+,y+
    inc     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_6000_7FFF
    inc     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_A000_BFFF
    ldw     #$2000
    ldx     #$6000
    ldy     #$A000
    tfm     x+,y+
    inc     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_6000_7FFF
    inc     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_A000_BFFF
    ldw     #$1A00
    ldx     #$6000
    ldy     #$A000
    tfm     x+,y+
    ;
    puls    a
    ldb     #LOGICAL_A000_BFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    puls    a
    decb
    lbsr    MapPhysicalBlockToLogicalBlock
    ;
    pulsw
    puls    a,b,x,y,pc

    


;***********************
BlitLevelToOffScreen
;***********************
    pshs    y,x,b,a
    pshsw
    ldd     <playfieldTopLeftY
    lsrd                                        ; ÷ 2 (shifts bit 0 of a into bit 7 of b)
    lsrb                                        ; ÷ 4
    lsrb                                        ; ÷ 8
    lsrb                                        ; b = low 9 bits of playfieldTopLeftY ÷ 16 (# of 8K banks (8k ÷ 512 bytes per line = 16) into prerendered level to start at)
    addb    #PRERENDERED_LEVEL_PHYSICAL_BLOCK
    tfr     b,a                                 ; a = Physical bank into prerended level to start at
    ; Map starting Physical bank into Logical block $6000-$7FFF (our scratch Logical block)
    ldb     #LOGICAL_6000_7FFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ; Map 1st Physical bank of off screen video buffer to Logical block $A000-$BFFF
    lda     videoBufferPhysicalBlockOffScreen,pcr
    ldb     #LOGICAL_A000_BFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ; Map 2nd Physical bank of off screen video buffer to Logical block $C000-$DFFF
    lda     videoBufferPhysicalBlockOffScreen,pcr
    inca
    incb
    lbsr    MapPhysicalBlockToLogicalBlock
    ; Calculate pointer from $6000 to the first byte of the 512 byte line we're going to start drawing from
    ldx     #$6000
    lda     <playfieldTopLeftY+1                ; a = playfieldTopLeftY % $FF
    clrb                                        ; d = playfieldTopLeftY * 256 by loading the low byte into a (MSB of d) and clearing b (LSB of d)
    anda    #$0F
    pshs    a                                   ; a = playfieldTopLeftY % 16 (16 = # of 512 byte lines in a Physical bank)
    lsld                                        ; d = (playfieldTopLeftY % 16) * 512 = Number of bytes of full 512 byte lines into the first Physical bank
    leax    d,x                                 ; x = pointer to first byte of 512 byte line in Physical bank
    ; Add the number of bytes into the first 512 byte line we're going to start drawing from
    ldd     <playfieldTopLeftX
    lsrd                                        ; ÷ 2 since there are 2 pixels per byte
    leax    d,x                                 ; x = pointer into $6000-$7FFF Logical bank we're going to start copying from
    ; 
    ldy     #$A000                              ; y = pointer to start of $A000-$BFFF Logical bank we're going to start copying to
    puls    a                                   ; a = number of 512 byte lines into Logical bank we're copying from
    tsta
    bne     @partialFirstBlock                  ; If it's 0, we're drawing the entire first block (8k). Otherwise, we're drawing a partial first block
    lda     #16                                 ; Copy 16 (all) lines in first block
    ldb     #9                                  ; Copy 9 blocks
    bra     @nextBlock
@partialFirstBlock
    coma
    anda    #$0F
    inca                                        ; a = 16 - (playfieldTopLeftY & $000F) = Number of lines to copy from first block
    ldb     #10                                 ; Copy 10 blocks
    lbra    @nextRowInPartialBlock
@nextBlock
    cmpa    #16
    lbne    @nextRowInPartialBlock
@copyWholeBlock
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    bra     @doneCopyBlock
@nextRowInPartialBlock
    ldw     #160
    tfm     x+,y+
    leax    512-160,x
    deca
    bne     @nextRowInPartialBlock
    ;
@doneCopyBlock
    cmpb    #2
    beq     @adjustForFinalRowCount
    lda     #16
    bra     @doneAdjustRowCount
@adjustForFinalRowCount
    lda     <playfieldTopLeftY+1
    anda    #$0F
    bne     @doneAdjustRowCount
    lda     #16
@doneAdjustRowCount
    ;
    inc     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_6000_7FFF
    * inc     mmu+LOGICAL_6000_7FFF,pcr
    ldx     #$6000
    ldw     <playfieldTopLeftX
    lsrw
    leax    w,x
    cmpy    #$C000
    blo     @doneAdjustScreenBlocks
    inc     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_A000_BFFF
    * inc     mmu+LOGICAL_A000_BFFF,pcr
    inc     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_C000_DFFF
    * inc     mmu+LOGICAL_C000_DFFF,pcr
    leay    -$2000,y
@doneAdjustScreenBlocks
    ;
    decb
    lbne    @nextBlock
    ;
    pulsw
    puls    a,b,x,y,pc

;***********************
PrerenderLevel
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    lda     #PRERENDERED_LEVEL_PHYSICAL_BLOCK
    ldb     #LOGICAL_A000_BFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    pshs    a
    lda     #PRERENDERED_LEVEL_PHYSICAL_BLOCK+1
    incb
    lbsr    MapPhysicalBlockToLogicalBlock
    pshs    a
    ldx     #$A000
    clre            ; Map X
    clrf            ; May Y
@nextTile
    ;|vvvvvvvv DEBUG vvvvvvvvv
    ;|                      ;|
    ;tfr     x,d             ;|
    ;lbsr    PrintHexWord    ;|
    ;lda     #32             ;|
    ;lbsr    PrintChar80x24  ;|
    ;|                      ;|
    ;|^^^^^^^^^^^^^^^^^^^^^^^^
    ; Calculate address of map cell and get tile #
    tfr     f,a
    ldb     #32
    mul
    leay    currentMap,pcr
    leay    d,y
    lda     e,y     ; a = Cell #
    leay    mapToTileLookup,pcr
    lda     a,y     ; a = Tile #
    ;
    lbsr    DrawTileIn512ByteWideBuffer
    leax    16,x
    ince
    cmpe    #32                     ; 32 tiles per row
    blo     @nextTile
    clre
    incf
    cmpf    #16                     ; 16 rows
    bhs     @doneTiles
    inc     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_A000_BFFF
    inc     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_A000_BFFF
    inc     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_C000_DFFF
    inc     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_C000_DFFF
    ldx     #$A000
    bra     @nextTile
@doneTiles
    ;
    puls    a
    ldb     #LOGICAL_C000_DFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    puls    a
    decb
    lbsr    MapPhysicalBlockToLogicalBlock
    pulsw
    puls    a,b,x,y,pc


;***********************
DrawTilesOffScreen
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    clre            ; Map X
    clrf            ; May Y
    lda     videoBufferPhysicalBlockOffScreen,pcr
    ldb     #LOGICAL_A000_BFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    pshs    a
    lda     videoBufferPhysicalBlockOffScreen,pcr
    inca
    incb
    lbsr    MapPhysicalBlockToLogicalBlock
    pshs    a
    ldx     #$A000
@nextTile
    cmpx    #$C000
    blo     @doneAdjustBlocks
    inc     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_A000_BFFF
    inc     MMU_BLOCK_REGISTERS_FIRST+LOGICAL_C000_DFFF
    leax    -$2000,x
@doneAdjustBlocks
    ;|vvvvvvvv DEBUG vvvvvvvvv
    ;|                      ;|
    ;tfr     x,d             ;|
    ;lbsr    PrintHexWord    ;|
    ;lda     #32             ;|
    ;lbsr    PrintChar80x24  ;|
    ;|                      ;|
    ;|^^^^^^^^^^^^^^^^^^^^^^^^
    ; Calculate address of map cell and get tile #
    tfr     f,a
    ldb     #32
    mul
    leay    currentMap,pcr
    leay    d,y
    lda     e,y     ; a = Cell #
    leay    mapToTileLookup,pcr
    lda     a,y     ; a = Tile #
    ;
    lbsr    DrawTileIn160ByteWideBuffer
    leax    16,x
    ince
    cmpe    #10                     ; 10 tiles per row
    blo     @nextTile
    leax    160*31,x
    adde    #-10
    incf
    cmpf    #5                      ; 5 rows
    blo     @nextTile
    ;
    puls    a
    ldb     #LOGICAL_C000_DFFF
    lbsr    MapPhysicalBlockToLogicalBlock
    puls    a
    decb
    lbsr    MapPhysicalBlockToLogicalBlock
    pulsw
    puls    a,b,x,y,pc

;***********************
DrawTileIn512ByteWideBuffer
; ```plaintext
; > a = Tile #
; > x = Logical address to start drawing
; - Uses $6000-$7FFF to map tile data in
; ```
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    ldb     mmu+LOGICAL_6000_7FFF,pcr
    pshs    b
    ; Calculate the physical block the tile is in by taking the high nibble (4 bits)
    ; of the tile # and adding the first block # of the tile data.
    ; ex: Tile # $6B is in physical block $10 (first block of tile data) + 6, or $16
    pshs    a
    lsra
    lsra
    lsra
    lsra
    adda    #TILE_DATA_PHYSICAL_BLOCK
    ldb     #LOGICAL_6000_7FFF
    lbsr    MapPhysicalBlockToLogicalBlock
    puls    a
    ; Calculate the offset into the tile #'s block where tile # starts by taking the
    ; low nibble of the tile # and multiplying by 512 (number of bytes in each tile).
    ; ex: Tile # $6B is at offset $B * 512, or $1600
    anda    #$0F
    lsla
    clrb                ; d now contains the tile # * 512
    ;
    ldy     #$6000
    leay    d,y
    ldb     #32         ; Draw 32 rows of the tile
@nextRow
    ldw     #16         ; Draw 16 bytes (32 pixels) in this row
    tfm     y+,x+
    leax    512-16,x    ; Adjust x (where we're drawing) down one row and back to the beginning column
    decb
    bne     @nextRow    ; Keep drawing until all 32 rows have been drawn
    ;
    puls    a
    ldb     #LOGICAL_6000_7FFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ;
    pulsw
    puls    a,b,x,y,pc

;***********************
DrawTileIn160ByteWideBuffer
; ```plaintext
; > a = Tile #
; > x = Logical address to start drawing
; - Uses $6000-$7FFF to map tile data in
; ```
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    ldb     mmu+LOGICAL_6000_7FFF,pcr
    pshs    b
    ; Calculate the physical block the tile is in by taking the high nibble (4 bits)
    ; of the tile # and adding the first block # of the tile data.
    ; ex: Tile # $6B is in physical block $10 (first block of tile data) + 6, or $16
    pshs    a
    lsra
    lsra
    lsra
    lsra
    adda    #TILE_DATA_PHYSICAL_BLOCK
    ldb     #LOGICAL_6000_7FFF
    lbsr    MapPhysicalBlockToLogicalBlock
    puls    a
    ; Calculate the offset into the tile #'s block where tile # starts by taking the
    ; low nibble of the tile # and multiplying by 512 (number of bytes in each tile).
    ; ex: Tile # $6B is at offset $B * 512, or $1600
    anda    #$0F
    lsla
    clrb                ; d now contains the tile # * 512
    ;
    ldy     #$6000
    leay    d,y
    ldb     #32         ; Draw 32 rows of the tile
@nextRow
    ldw     #16         ; Draw 16 bytes (32 pixels) in this row
    tfm     y+,x+
    leax    160-16,x    ; Adjust x (where we're drawing) down one row and back to the beginning column
    decb
    bne     @nextRow    ; Keep drawing until all 32 rows have been drawn
    ;
    puls    a
    ldb     #LOGICAL_6000_7FFF
    lbsr    MapPhysicalBlockToLogicalBlock
    ;
    pulsw
    puls    a,b,x,y,pc

;***********************
SwapOnScreenAndOffScreenBuffers
;***********************
    pshs    y,x,b,a
    ;
    ; Swap On/Off Screen buffer first Physical blocks
    ldd     videoBufferPhysicalBlockOnScreen,pcr
    exg     a,b
    std     videoBufferPhysicalBlockOnScreen,pcr
    ;
    ; Swap GIME Vertical Offset values for On/Off Screen buffer
    ldx     verticalOffsetOnScreen,pcr
    ldy     verticalOffsetOffScreen,pcr
    exg     x,y
    stx     verticalOffsetOnScreen,pcr
    sty     verticalOffsetOffScreen,pcr
    stx     GIME_VerticalOffset1_FF9D
    ;
    puls    a,b,x,y,pc

    END     start
