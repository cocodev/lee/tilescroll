╔════════════════┌───────────────────────────────────────┐════════════════╗
║Payfield   00000│Memory (512x512)                       │                ║
║(1024x512) 02000│      ┌----------------------┐         │                ║
║           04000│      !Screen (320x200)      !         │                ║
║           06000│      !                      !         │                ║
║           08000│      !                      !         │                ║
║           0A000│      !----------------------!         │                ║
║           0C000│      ! Stationary Score     !         │                ║
║           0E000│      └----------------------┘         │                ║
║           10000│                                       │                ║
║           12000│                                       │                ║
║           14000│                                       │                ║
║           16000│                                       │                ║
║           18000│           ┌----------------------┐    │                ║
║           1A000│           !Screen (320x200)      !    │                ║
║           1C000│           !                      !    │                ║
║           1E000│           !                      !    │                ║
╚════════════════└───────────!----------------------!────┘════════════════╝
                             ! Stationary Score     !
                             └----------------------┘

│ ┤ ╡ ╢ ╖ ╕ ╣ ║ ╗ ╝ ╜ ╛ ┐

└ ┴ ┬ ├ ─ ┼ ╞ ╟ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╧

╨ ╤ ╥ ╙ ╘ ╒ ╓ ╫ ╪ ┘ ┌   


$00 = $00000 - $01FFF          $20    $40000 - $41FFF                     $00 = $0000 - $1FFF x
$01 = $02000 - $03FFF          $21    $42000 - $43FFF                     $01 = $2000 - $3FFF x
$02 = $04000 - $05FFF          $22    $44000 - $45FFF                     $02 = $4000 - $5FFF
$03 = $06000 - $07FFF          $23    $46000 - $47FFF                     $03 = $6000 - $7FFF x
$04 = $08000 - $09FFF          $24    $48000 - $49FFF                     $04 = $8000 - $9FFF
$05 = $0A000 - $0BFFF          $25    $4A000 - $4BFFF                     $05 = $A000 - $BFFF
$06 = $0C000 - $0DFFF          $26    $4C000 - $4DFFF                     $06 = $C000 - $DFFF
$07 = $0E000 - $0FFFF          $27    $4E000 - $4FFFF                     $07 = $E000 - $FFFF x
$08 = $10000 - $11FFF          $28    $50000 - $51FFF
$09 = $12000 - $13FFF          $29    $52000 - $53FFF
$0A = $14000 - $15FFF          $2A    $54000 - $55FFF
$0B = $16000 - $17FFF          $2B    $56000 - $57FFF
$0C = $18000 - $19FFF          $2C    $58000 - $59FFF
$0D = $1A000 - $1BFFF          $2D    $5A000 - $5BFFF
$0E = $1C000 - $1DFFF          $2E    $5C000 - $5DFFF
$0F = $1E000 - $1FFFF          $2F    $5E000 - $5FFFF
$10 = $20000 - $21FFF          $30    $60000 - $61FFF
$11 = $22000 - $23FFF          $31    $62000 - $63FFF
$12 = $24000 - $25FFF          $32    $64000 - $65FFF
$13 = $26000 - $27FFF          $33    $66000 - $67FFF
$14 = $28000 - $29FFF          $34    $68000 - $69FFF
$15 = $2A000 - $2BFFF          $35    $6A000 - $6BFFF
$16 = $2C000 - $2DFFF          $36    $6C000 - $6DFFF
$17 = $2E000 - $2FFFF          $37    $6E000 - $6FFFF
$18 = $30000 - $31FFF          $38    $70000 - $71FFF
$19 = $32000 - $33FFF          $39    $72000 - $73FFF
$1A = $34000 - $35FFF          $3A    $74000 - $75FFF
$1B = $36000 - $37FFF          $3B    $76000 - $77FFF
$1C = $38000 - $39FFF          $3C    $78000 - $79FFF
$1D = $3A000 - $3BFFF          $3D    $7A000 - $7BFFF
$1E = $3C000 - $3DFFF          $3E    $7C000 - $7DFFF
$1F = $3E000 - $3FFFF          $3F    $7E000 - $7FFFF





$00 = $00000 - $01FFF   =   Screen Buffer 1
$01 = $02000 - $03FFF   =   Screen Buffer 1
$02 = $04000 - $05FFF   =   Screen Buffer 1
$03 = $06000 - $07FFF   =   Screen Buffer 1     ($06000-$07CFF)
$04 = $08000 - $09FFF   =   Screen Buffer 2
$05 = $0A000 - $0BFFF   =   Screen Buffer 2
$06 = $0C000 - $0DFFF   =   Screen Buffer 2
$07 = $0E000 - $0FFFF   =   Screen Buffer 2     ($0E000-$0ECFF)
$08 = $10000 - $11FFF   =   Tile Data (0-15)    (16 tiles)
$09 = $12000 - $13FFF   =   Tile Data (17-31)   (16 tiles)
$0A = $14000 - $15FFF   =   Tile Data (32-47)   (16 tiles)
$0B = $16000 - $17FFF   =   Tile Data (48-63)   (16 tiles)
$0C = $18000 - $19FFF   =   Tile Data (64-79)   (16 tiles)
$0D = $1A000 - $1BFFF   =   Tile Data (80-95)   (16 tiles)
$0E = $1C000 - $1DFFF   =   Tile Data (96-111)  (16 tiles)
$0F = $1E000 - $1FFFF   =   Tile Data (112-119) (8 tiles, $1E000-$1EFFF)
$10 = $20000 - $21FFF   =   Predrawn Level - Top    row 1                   Won't work, can't blit since rows are different widths.
$11 = $22000 - $23FFF   =   Predrawn Level - Bottom row 1                   Would have to do 144 blits of 160 bytes each. Might
$12 = $24000 - $25FFF   =   Predrawn Level - Top    row 2                   still be worth a try.
$13 = $26000 - $27FFF   =   Predrawn Level - Bottom row 2
$14 = $28000 - $29FFF   =   Predrawn Level - Top    row 3
$15 = $2A000 - $2BFFF   =   Predrawn Level - Bottom row 3
$16 = $2C000 - $2DFFF   =   Predrawn Level - Top    row 4
$17 = $2E000 - $2FFFF   =   Predrawn Level - Bottom row 4
$18 = $30000 - $31FFF   =   Predrawn Level - Top    row 5
$19 = $32000 - $33FFF   =   Predrawn Level - Bottom row 5
$1A = $34000 - $35FFF   =   Predrawn Level - Top    row 6
$1B = $36000 - $37FFF   =   Predrawn Level - Bottom row 6
$1C = $38000 - $39FFF   =   Predrawn Level - Top    row 7
$1D = $3A000 - $3BFFF   =   Predrawn Level - Bottom row 7
$1E = $3C000 - $3DFFF   =   Predrawn Level - Top    row 8
$1F = $3E000 - $3FFFF   =   Predrawn Level - Bottom row 8
$20 = $40000 - $41FFF   =   Predrawn Level - Top    row 9
$21 = $42000 - $43FFF   =   Predrawn Level - Bottom row 9
$22 = $44000 - $45FFF   =   Predrawn Level - Top    row 10
$23 = $46000 - $47FFF   =   Predrawn Level - Bottom row 10
$24 = $48000 - $49FFF   =   Predrawn Level - Top    row 11
$25 = $4A000 - $4BFFF   =   Predrawn Level - Bottom row 11
$26 = $4C000 - $4DFFF   =   Predrawn Level - Top    row 12
$27 = $4E000 - $4FFFF   =   Predrawn Level - Bottom row 12
$28 = $50000 - $51FFF   =   Predrawn Level - Top    row 13
$29 = $52000 - $53FFF   =   Predrawn Level - Bottom row 13
$2A = $54000 - $55FFF   =   Predrawn Level - Top    row 14
$2B = $56000 - $57FFF   =   Predrawn Level - Bottom row 14
$2C = $58000 - $59FFF   =   Predrawn Level - Top    row 15
$2D = $5A000 - $5BFFF   =   Predrawn Level - Bottom row 15
$2E = $5C000 - $5DFFF   =   Predrawn Level - Top    row 16
$2F = $5E000 - $5FFFF   =   Predrawn Level - Bottom row 16
$30 = $60000 - $61FFF
$31 = $62000 - $63FFF
$32 = $64000 - $65FFF
$33 = $66000 - $67FFF
$34 = $68000 - $69FFF
$35 = $6A000 - $6BFFF
$36 = $6C000 - $6DFFF
$37 = $6E000 - $6FFFF
$38 = $70000 - $71FFF
$39 = $72000 - $73FFF
$3A = $74000 - $75FFF
$3B = $76000 - $77FFF
$3C = $78000 - $79FFF
$3D = $7A000 - $7BFFF
$3E = $7C000 - $7DFFF
$3F = $7E000 - $7FFFF



Map Data (Level 0)
------------------
14 10 10 10 15 56 56 56 56 56    56 56 56 56 56 56 56 56 56 56 56 56 56 56 56 56 14 10 10 10 10 15 
12 49 01 01 06 01 01 01 01 01    01 01 01 01 00 01 01 01 00 01 01 01 00 01 01 01 06 01 2A 00 49 13 
12 01 01 01 2F 01 01 01 07 05    05 05 08 01 00 01 00 01 00 01 00 01 00 01 00 49 06 01 22 01 35 13 
12 01 01 01 06 01 01 01 06 01    01 01 06 01 01 01 00 01 01 01 00 01 01 01 00 46 06 2C 01 2A 01 13 
16 05 30 05 0F 05 05 05 0E 01    46 01 0D 05 05 05 10 05 05 05 10 05 05 05 10 05 0A 01 28 22 35 13 
56 01 01 01 06 46 49 46 06 01    01 01 06 48 01 01 01 01 01 01 01 01 01 01 01 01 01 01 00 01 01 13 
56 01 01 01 06 01 00 01 0D 05    05 05 0C 05 08 01 07 05 05 05 05 05 05 05 05 05 05 05 10 05 05 17 
56 01 01 01 06 28 00 27 06 46    01 49 01 46 06 01 06 4A 01 01 01 01 01 01 01 01 01 01 01 01 01 56 
56 32 01 01 06 28 00 27 06 01    00 00 00 01 06 01 06 01 00 00 00 00 00 00 00 00 00 00 00 00 00 56 
56 01 01 01 06 01 00 01 06 01    00 00 00 01 06 01 06 01 00 46 00 00 00 00 00 00 47 00 00 46 00 56 
56 01 01 01 2F 01 01 01 06 01    00 00 00 01 06 4B 06 01 00 00 00 00 00 00 00 00 00 00 00 00 00 56 
56 01 01 01 09 05 05 05 0A 01    00 00 00 01 09 30 0A 01 00 00 00 47 00 00 00 00 00 00 00 00 00 56 
56 01 01 01 01 31 31 31 31 31    00 00 00 01 01 01 01 01 00 00 00 00 00 00 00 00 00 00 00 00 00 56 
56 01 01 01 01 31 31 31 31 31    00 00 00 01 01 32 01 47 00 00 00 00 00 46 00 00 00 00 00 00 46 56 
56 01 01 01 01 31 31 31 31 31    01 01 01 01 01 32 01 01 00 00 00 00 00 00 00 00 00 47 00 00 00 56 
56 56 56 56 56 56 56 56 56 56    56 56 56 56 56 56 56 56 56 56 56 56 56 56 56 56 56 56 56 56 56 56

Tile #
------
00 04 04 04 09 6E 6E 6E 6E 6E    6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 00 04 04 04 04 09 
01 64 6B 6B 14 6B 6B 6B 6B 6B    6B 6B 6B 6B 25 6B 6B 6B 25 6B 6B 6B 25 6B 6B 6B 14 6B 0B 25 64 1E 
01 6B 6B 6B 0A 6B 6B 6B 1A 06    06 06 1C 6B 25 6B 25 6B 25 6B 25 6B 25 6B 25 64 14 6B 2A 6B 13 1E 
01 6B 6B 6B 14 6B 6B 6B 14 6B    6B 6B 14 6B 6B 6B 25 6B 6B 6B 25 6B 6B 6B 25 69 14 0F 6B 0B 6B 1E 
02 06 07 06 16 06 06 06 17 6B    69 6B 19 06 06 06 04 06 06 06 04 06 06 06 04 06 08 6B 0D 2A 13 1E 
6E 6B 6B 6B 14 69 64 69 14 6B    6B 6B 14 67 6B 6B 6B 6B 6B 6B 6B 6B 6B 6B 6B 6B 6B 6B 25 6B 6B 1E 
6E 6B 6B 6B 14 6B 25 6B 19 06    06 06 15 06 1C 6B 1A 06 06 06 06 06 06 06 06 06 06 06 04 06 06 1D 
6E 6B 6B 6B 14 0D 25 0C 14 69    6B 64 6B 69 14 6B 14 65 6B 6B 6B 6B 6B 6B 6B 6B 6B 6B 6B 6B 6B 6E 
6E 6B 6B 6B 14 0D 25 0C 14 6B    25 25 25 6B 14 6B 14 6B 25 25 25 25 25 25 25 25 25 25 25 25 25 6E 
6E 6B 6B 6B 14 6B 25 6B 14 6B    25 25 25 6B 14 6B 14 6B 25 69 25 25 25 25 25 25 6A 25 25 69 25 6E 
6E 6B 6B 6B 0A 6B 6B 6B 14 6B    25 25 25 6B 14 66 14 6B 25 25 25 25 25 25 25 25 25 25 25 25 25 6E 
6E 6B 6B 6B 1B 06 06 06 08 6B    25 25 25 6B 1B 07 08 6B 25 25 25 6A 25 25 25 25 25 25 25 25 25 6E 
6E 6B 6B 6B 6B 03 03 03 03 03    25 25 25 6B 6B 6B 6B 6B 25 25 25 25 25 25 25 25 25 25 25 25 25 6E 
6E 6B 6B 6B 6B 03 03 03 03 03    25 25 25 6B 6B 6B 6B 6A 25 25 25 25 25 69 25 25 25 25 25 25 69 6E 
6E 6B 6B 6B 6B 03 03 03 03 03    6B 6B 6B 6B 6B 6B 6B 6B 25 25 25 25 25 25 25 25 25 6A 25 25 25 6E 
6E 6E 6E 6E 6E 6E 6E 6E 6E 6E    6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E 6E
